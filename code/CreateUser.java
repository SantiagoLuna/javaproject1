/**
 * @authors Santiago Luna & Maria Maria Ramirez
 * This class defines user objects. It is in charge of permanently appending the username and password entered by the user to the fule Users.txt
 * With the newly aquired credentials, this class also creates a new User object so that the new user information is now logged as a normal User forever.
 */
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class CreateUser extends User implements Credentials{

    public CreateUser(String username, String password) {
        super(username, password);
    }

    public static void addToFile(String[] args) {
        //must read from file to keep copy before writing again
        try{   
            Path p = Paths.get(".\\files\\Users.txt"); //change file name for final product
            
            //check if file exists
            if(Files.exists(p)){
                System.out.println("file exists");
                List<String> fileLines = Files.readAllLines(p);
                fileLines.add(args[0]);
                fileLines.add(args[1]);
                Files.write(p, fileLines);
                //checks if file does not exist
             }else if(Files.notExists(p)){
                List<String> userIN = new ArrayList<String>();
                userIN.add(args[0]);
                userIN.add(args[1]);
                Files.write(p, userIN);
             }
                
        }catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    //@Override
    //this is only for the console
    public static String[] input() {
        String username, password;
        String[] cred = new String[2];

        Scanner reader = new Scanner(System.in);
        System.out.println("NEW USER..........");
        System.out.println("entner username: ");
        username = reader.nextLine();
        System.out.println("entner password: ");
        password = reader.nextLine();
        reader.close();
        cred[0] = username;
        cred[1] = password;

        //addToFile(cred);
        return cred;
    }

    //shoudlnt create the user here
    public static User create(String[] cred){
    User u = new User(cred[0], cred[1]);
    return u;
    }

    public static void run(){
        addToFile(input());
    }
}