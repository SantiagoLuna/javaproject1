/**
 * This class is in charge of creating Server and Client instances so that the chat between both the host and the client is propperly initialized. 
 * @authors Santiago Luna & Maria Maria Ramirez
 */
public class NormalChat extends Chat{

    public NormalChat(User u) {
        super(u);
    }

    @Override
    public void runServer(){
        System.out.println("Starting regular chat, it's being logged");
        Server.startServer(this.u, 1);
    }

    @Override
    public void runClient(String host){
        Client.startClient(this.u, host, 1);
    }
    
}
