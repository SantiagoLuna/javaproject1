/**
 * @authors Santiago Luna & Maria Maria Ramirez
 * This class assigns event handlers to the interactive fields in the App
 */
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class CreateUserEvent implements EventHandler<ActionEvent>{
    private TextField username;
    private TextField password;
    private String[] cred = new String[2];
    private Stage stage;
    private Scene next;

    public CreateUserEvent(TextField usr, TextField pass, Stage stage, Scene next){
        this.username = usr;
        this.password = pass;
        this.stage = stage;
        this.next = next;
    }

    @Override
    public void handle(ActionEvent e) {
        cred[0] = this.username.getText();
        cred[1] = this.password.getText();
        System.out.println(cred[0] + " " + cred[1]);
        CreateUser.addToFile(cred);
        System.out.println("created user");
        stage.setScene(this.next);
    }
    
}
