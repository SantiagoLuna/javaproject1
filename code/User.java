/**
 * @authors Santiago Luna & Maria Maria Ramirez
 */
public class User{
    protected String username;
    protected String password;

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public String getUsername(){
        return this.username;
    }
    public void setUsername(String uname){
        this.username = uname;
    }

    public void setPassword(String pass){
        this.password = pass;
    }

    public String getPassword(){
        return this.password;
    }
    
    public String toString(){
        return "Username: " + this.username + ", password: " + this.password;
    }
}