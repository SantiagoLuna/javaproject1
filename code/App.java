/**
 * @authors Santiago Luna & Maria Maria Ramirez
 * This class sets up the java FX file to display the GUI of the program
 */
import java.awt.Color;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.InputEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class App extends Application {
    //protected User u;

    private static User u = new User("username", "password");

    //to be called from within login event to set the user obj
    public static void setUser(String username, String password){
        App.u.setPassword(password);
        App.u.setUsername(username);
    }
    public static User getUser(){
        return App.u;
    }
    @Override
    public void start(Stage stage) {
        TilePane root = new TilePane();
    
        stage.setTitle("Chat App");

        stage.setScene(CreateUser(stage));
        
        //root.setAlignment(Pos.CENTER);
        stage.show();
    }

    //first scene creating the user
    public static Scene CreateUser(Stage stage){
        
        VBox root = new VBox();
        VBox text = new VBox();
        Scene s = new Scene(root, 640, 480);

        text.setPadding(new Insets(10,50,50,50));

        Text header = new Text("Create User");
        header.setFont(Font.font("Comic Sans MS", 50));
        text.getChildren().add(header);

        TextField username = new TextField("username");
        Label l1 = new Label("Enter username:");

        PasswordField password = new PasswordField();
        Label l2 = new Label("Enter password:");

        l1.setLabelFor(username);
        l2.setLabelFor(password);

        Button submit = new Button("Submit");
        Button clear = new Button("clear");
        
        root.getChildren().add(text);
        root.getChildren().add(l1);
        root.getChildren().add(username);
        
        root.getChildren().add(l2);
        root.getChildren().add(password);
        root.getChildren().add(submit);
        root.getChildren().add(clear);
        root.setAlignment(Pos.CENTER);

        CreateUserEvent event = new CreateUserEvent(username, password, stage, Login(stage));
        submit.setOnAction(event);
        root.setStyle("-fx-background-image: url('bg.png'); -fx-background-repeat: no-repeat;-fx-background-size: 1920 1080;-fx-background-position: center center;");

        return s;
    }
    //second scene logging in
    public static Scene Login(Stage stage){
         VBox root = new VBox();
         VBox text = new VBox();
         Scene s = new Scene(root, 640, 480);

        text.setPadding(new Insets(10,50,50,50));

        Text header = new Text("LOGIN");
        header.setFont(Font.font("Comic Sans MS", 50));
        text.getChildren().add(header);
        text.setAlignment(Pos.CENTER);

        TextField username = new TextField("username");
        Label l1 = new Label("Enter username:");

        PasswordField password = new PasswordField();
        Label l2 = new Label("Enter password:");

        l1.setLabelFor(username);
        l2.setLabelFor(password);

        Button submit = new Button("Submit");
        LoginEvent event = new LoginEvent(username, password, header, stage, home(stage));
        submit.setOnAction(event);
        System.out.println(u.getUsername());
        
        root.getChildren().add(text);
        root.getChildren().add(l1);
        root.getChildren().add(username);
        
        root.getChildren().add(l2);
        root.getChildren().add(password);
        root.getChildren().add(submit);
        root.setAlignment(Pos.CENTER);
        
        
        root.setStyle("-fx-background-image: url('bg.png'); -fx-background-repeat: no-repeat;-fx-background-size: 1920 1080;-fx-background-position: center center;");
        return s;
    }

    public static Scene home(Stage stage){

        VBox root = new VBox();        
        ToggleGroup rButtons = new ToggleGroup();
        Scene s = new Scene(root, 640, 480);
        

        Text name = new Text("WELCOME USER");
        name.setFont(Font.font("Comic Sans MS", 50));

        Button b1 = new Button("Local Chat");
        Button b2 = new Button("Normal Chat");
        Button b3 = new Button("Private Chat");

        RadioButton rb1 = new RadioButton("host");
        RadioButton rb2 = new RadioButton("connect");
        rb1.setToggleGroup(rButtons);
        rb2.setToggleGroup(rButtons);
        TextField ip = new TextField("Enter ip");

        RunChatsEvent e1 = new RunChatsEvent(rb1, rb2, b1, ip);
        RunChatsEvent e2 = new RunChatsEvent(rb1, rb2, b2, ip);
        RunChatsEvent e3 = new RunChatsEvent(rb1, rb2, b3, ip);
        b1.setOnAction(e1);
        b2.setOnAction(e2);
        b3.setOnAction(e3);


        root.getChildren().add(name);
        root.getChildren().add(b1);
        root.getChildren().add(b2);
        root.getChildren().add(b3);
        root.getChildren().add(rb1);
        root.getChildren().add(rb2);
        root.getChildren().add(ip);
        root.setAlignment(Pos.CENTER);
        VBox.setMargin(b1, new Insets(10,10,10,10));
        VBox.setMargin(b2, new Insets(10,10,10,10));
        VBox.setMargin(b3, new Insets(10,10,10,10));
        root.setStyle("-fx-background-image: url('bg.png'); -fx-background-repeat: no-repeat;-fx-background-size: 1920 1080;-fx-background-position: center center;");
        return s;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
