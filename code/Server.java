import java.io.BufferedReader;
import java.io.IOException;
/**
 * @authors Santiago Luna & Maria Maria Ramirez
 * This class is in charge of setting up the server of either the private or normal chats. This class also logs the user input into a generated file called ChatLog(count).txt
 */
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class Server {
    public static void startServer(User u, int type){
        final ServerSocket serverSocket;
        final Socket clientSocket;
        final BufferedReader in;
        final PrintWriter out;
        final Scanner reader = new Scanner(System.in);
        final int port = 3000;
        final Path p = createFile();
        reader.close();

        try {
            serverSocket = new ServerSocket(port);
            clientSocket = serverSocket.accept();
            //output of the application
            out = new PrintWriter(clientSocket.getOutputStream());
            //input to the server
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            Thread sender = new Thread(new Runnable() {
                String msg;

                @Override
                public void run(){
                    while(true){
                        msg = reader.nextLine(); //gets input 
                        if(!(type == 2)){
                            try{
                                writeToFile(p, u.getUsername() + ": "+ msg, u);
                            }catch(IOException e){
                                e.printStackTrace();
                            }
                        }
                        out.println(u.getUsername() + ": "+ msg); //store input in clientsocket
                        out.flush(); //sends the data
                    }
                }
            });
            sender.start();

            Thread recieve = new Thread(new Runnable() {
               String msg;
               
               @Override
               public void run() {
                   try{
                       System.out.println("Server has started");
                       msg = in.readLine(); //read message from client socket
                       //while the client  is still connected to the server
                       while(msg!=null){
                           System.out.println(msg);
                           msg = in.readLine();
                           if(!(type == 2)){
                            try{
                                writeToFile(p, msg, u);
                            }catch(IOException e){
                                e.printStackTrace();
                            }
                        }
                       }
                       //if msg is null then client isnt connected to the server anymore
                       System.out.println("client disconnected");
                       //closing sockets
                       out.close();
                       clientSocket.close();
                       serverSocket.close();
                   }catch(IOException e){
                       e.printStackTrace();
                   }
               }
            });
            recieve.start();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static Path createFile(){
        int count = 0;
        Path path = Paths.get(".\\files\\ChatLog"+count+".txt");
        while(Files.exists(path)){
            count++;
            path = Paths.get(".\\files\\ChatLog"+count+".txt");
        }
        return path;
    }

    //reading and logging user input to file
    public static void writeToFile(Path path,String msg, User u) throws IOException{
        
        if(Files.exists(path)){
            List<String> fileLines = Files.readAllLines(path);
            fileLines.add(msg);
            Files.write(path, fileLines);
        }else{
            List<String> userIN = new ArrayList<String>();
            userIN.add(msg);
            Files.write(path, userIN);
        }

    }
}
