/**
 * @author Santiago Luna & Maria Maria Ramirez 
 * This class is in charge of setting up the stage of the GUI which will carry out the visual aspect of the chat program.
 */
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class RunChatsEvent implements EventHandler<ActionEvent>{
    private RadioButton host;
    private RadioButton connect;
    private Button chat;
    private TextField ip;
    
    public RunChatsEvent(RadioButton r1, RadioButton r2, Button b1, TextField ip){
        this.host = r1;
        this.connect = r2;
        this.chat = b1;
        this.ip = ip;
    }

    @Override
    public void handle(ActionEvent arg0) {
        if(host.isSelected() && chat.getText().equals("Local Chat")){
            System.out.println("Pressed Local Chat + host");

            System.out.println(App.getUser().getUsername());
            
        }
        else if(host.isSelected() && chat.getText().equals("Private Chat")){
            System.out.println("Pressed Private Chat + host");
            PrivateChat p = new PrivateChat(App.getUser());
            Label secondLabel = new Label("Private Chat");

            VBox root = new VBox();
            HBox inputField = new HBox();
            Scene s = new Scene(root, 640, 480);

            //input from user
            TextField input = new TextField("enter a message!");
            Button send = new Button("Send");
            inputField.getChildren().addAll(input, send);

            //the chat that the user is having
            TextArea chat = new TextArea("nothing yet");
            chat.setEditable(false);

            Text error = new Text("NON IMPLIMENTED FEATURES YET, PLEASE GO TO THE COMMAND LINE TO USE THE CHAT");

            root.getChildren().add(chat);
            root.getChildren().add(inputField);
            root.getChildren().add(error);

			StackPane secondaryLayout = new StackPane(root);
			secondaryLayout.getChildren().add(secondLabel);
			Scene secondScene = new Scene(secondaryLayout, 640, 480);
			// New window (Stage)
			Stage newWindow = new Stage();
			newWindow.setTitle("Private Chat");
			newWindow.setScene(secondScene);

			newWindow.show();
            p.runServer();
        }
        else if(host.isSelected() && chat.getText().equals("Normal Chat")){
            System.out.println("Pressed Normal Chat + host");
            NormalChat n = new NormalChat(App.getUser());
            Label secondLabel = new Label("Private Chat");

				StackPane secondaryLayout = new StackPane();
				secondaryLayout.getChildren().add(secondLabel);

				Scene secondScene = new Scene(secondaryLayout, 640, 480);

				// New window (Stage)
				Stage newWindow = new Stage();
				newWindow.setTitle("Normal Chat");
				newWindow.setScene(secondScene);

				newWindow.show();
            n.runServer();


        }
        else if(connect.isSelected() && chat.getText().equals("Local Chat")){
            System.out.println("Pressed Local Chat + connect");
        }
        else if(connect.isSelected() && chat.getText().equals("Private Chat")){
            System.out.println("Pressed Private Chat + connect");
            PrivateChat p = new PrivateChat(App.getUser());
            Label secondLabel = new Label("Private Chat");

				StackPane secondaryLayout = new StackPane();
				secondaryLayout.getChildren().add(secondLabel);

				Scene secondScene = new Scene(secondaryLayout, 640, 480);

				// New window (Stage)
				Stage newWindow = new Stage();
				newWindow.setTitle("Private Chat");
				newWindow.setScene(secondScene);

				newWindow.show();
            p.runClient(this.ip.getText());
        }
        else if(connect.isSelected() && chat.getText().equals("Normal Chat")){
            System.out.println("Pressed Normal Chat + connect");
            NormalChat n = new NormalChat(App.getUser());
            Label secondLabel = new Label("Private Chat");

				StackPane secondaryLayout = new StackPane();
				secondaryLayout.getChildren().add(secondLabel);

				Scene secondScene = new Scene(secondaryLayout, 640, 480);

				// New window (Stage)
				Stage newWindow = new Stage();
				newWindow.setTitle("Normal Chat");
				newWindow.setScene(secondScene);

				newWindow.show();
            n.runClient(this.ip.getText());
        }
    }
}
