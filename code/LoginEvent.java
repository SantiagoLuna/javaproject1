/**
 * @authors Santiago Luna & Maria Maria Ramirez
 * This class is in charge of creating the handles and events that occur when buttons in the gui are clicked and interacted with. 
 */

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LoginEvent implements EventHandler<ActionEvent>{
    private TextField username;
    private TextField password;
    private String[] cred = new String[2];
    private Stage stage;
    private Scene scene;
    private Text text;

    public LoginEvent(TextField usr, TextField pass, Text header, Stage stage, Scene next){
        this.username = usr;
        this.password = pass;
        this.text = header;
        this.stage = stage;
        this.scene = next;

    }

    @Override
    public void handle(ActionEvent e) {
        cred[0] = this.username.getText();
        cred[1] = this.password.getText();
        System.out.println(cred[0] + " " + cred[1]);
        try{
            if(!Login.validate(this.cred)){
                System.out.println("not a user!");
                this.username.setStyle("-fx-text-box-border: #B22222; -fx-focus-color: #B22222;");
                this.password.setStyle("-fx-text-box-border: #B22222; -fx-focus-color: #B22222;");
        }else{
                System.out.println("Logged in");
                this.username.setStyle("-fx-text-box-border: #00FF00; -fx-focus-color: #00FF00;");
                this.password.setStyle("-fx-text-box-border: #00FF00; -fx-focus-color: #00FF00;");

                App.setUser(cred[0], cred[1]);
                
                
                //App.uName = "MY NAME";
                Alert alert = new Alert(AlertType.NONE);
                alert.setAlertType(AlertType.WARNING);
                alert.setContentText("FEATURES NOT IMPLIMENTED YET, PLEASE CHOOSE A CHAT OPTION THEN GO TO THE COMMAND LINE TO USE THE CHAT");
                alert.show();
                //this.u = Login.makeUser();
                stage.setScene(scene);
        }
        }catch(IOException a){
            System.out.println("invalid input");
        }
        
    }
}
