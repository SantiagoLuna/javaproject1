/**
 * @authors Santiago Luna & Maria Maria Ramirez
 * 
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public abstract class Client {
    public static void startClient(User u, String host, int type){
        final Socket clientSocket;
        final BufferedReader in;
        final PrintWriter out;
        final Scanner reader = new Scanner(System.in);
        final String ip = host;
        final int port = 3000;
        reader.close();
        try{
            clientSocket = new Socket(ip, port);
            out = new PrintWriter(clientSocket.getOutputStream());
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        //try{
            Thread sender = new Thread(new Runnable() {
                String msg;

                @Override
                public void run(){
                    while(true){
                        msg = reader.nextLine(); //gets input
                        
                        out.println(u.getUsername()+": "+msg); //store input in clientsocket
                        out.flush(); //sends the data
                    }
                }
            });
            sender.start();

            Thread recieve = new Thread(new Runnable() {
                String msg;
                
                @Override
                public void run() {
                    try{
                        msg = in.readLine(); //read message from client socket
                        //while the client  is still connected to the server
                        while(msg!=null){
                            System.out.println(msg);
                            msg = in.readLine();
                        }
                        //if msg is null then client isnt connected to the server anymore
                        System.out.println("Server down");
                        //closing sockets
                        out.close();
                        clientSocket.close();
                    }catch(IOException e){
                        e.printStackTrace();
                    }
                }
             });
             recieve.start();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
