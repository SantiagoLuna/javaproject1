/**
 * @authors Santiago Luna & Maria Maria Ramirez
 * This class allows the user to start a chat server as well as creates an instance for the client (the "other side" of the chat)
 * This class also sets the host for the chat ip address
 */
import java.util.Scanner;

public abstract class Chat {
    String host;
    User u;

    public Chat(User u){
        this.host = "";
        this.u = u;
    }

    public void runServer(){
        Server.startServer(this.u, 1);
    }

    public void runClient(String host){
        Client.startClient(this.u, host, 1);
    }

    public String setHost(){
        Scanner reader = new Scanner(System.in);
        System.out.print("enter the hosts ip address: ");
        String ip = reader.next();
        reader.close();
        return ip;
    }
}
