/**
 * @authors Santiago Luna & Maria Maria Ramirez
 * This class is the entire functionality of the local chat. It is in charge of asking the user to input a message. It then carries the message over to the send method
 * and appends the message to a file called localChat.txt. After this message is sent and appended to the file, the file then calls the recieve method which is in charge
 * of reading the localChat file and displaying its contents. This means it will display the message the user just sent to the computer and all the messages sent previously.
 */
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Local{

    public void input() throws IOException {
        recieve();
        Scanner input = new Scanner(System.in);
        System.out.println("Say something");
        String message = input.nextLine();
        send(message);
        input.close();
    }

    public void send(String message) throws IOException {
        Path p = Paths.get("files/localChat.txt");

        if(Files.exists(p)){
            List<String> fileChat = Files.readAllLines(p);
            fileChat.add(message);
            Files.write(p, fileChat);
        }
        else{
            List<String> messages = new ArrayList<String>();
            messages.add(message);
            Files.write(p, messages);
        }
        recieve();
    }

    public void recieve() throws IOException{
        Path p = Paths.get("files/localChat.txt");
        List<String> chat = Files.readAllLines(p);
        for(String m : chat){
            System.out.println(m);
        }
    }
}