/**
 * This class is in charge of creating Server and Client instances so that the chat between both the the client and the computer is propperly initialized.
 * @authors Santiago Luna & Maria Maria Ramirez
 */
public class PrivateChat extends Chat{
    Chat c;
    public PrivateChat(User u) {
        super(u);
    }

    @Override
    public void runServer(){
        System.out.println("Private chat is starting...");
        Server.startServer(this.u, 2);
    }

    @Override
    public void runClient(String host){
        Client.startClient(this.u, host, 2);
    }
    
}
