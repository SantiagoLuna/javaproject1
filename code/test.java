/**
 * @authors Santiago Luna & Maria Maria Ramirez
 * This class contains the main method of the whole program. It is ultimately used for testing the program. It is in charge of carrying out the logic of the log in process and when the kinds
 * of chats get prompted accoording to the user input. 
 */
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class test {
    public static void main(String[] args) throws IOException{
        PrivateChat pc;
        NormalChat nc;
        
        Scanner reader = new Scanner(System.in);
        //prompting the user if they are logging in for the first time or not
        System.out.println("new user?(Y / N)");
        String in = reader.nextLine(); 
        if(in.equals("Y")){
            CreateUser.run();
            Login.run();
            User u = Login.makeUser();

            //run chat app
            System.out.println("do you want to chat, private chat, chat with computer (type 1, 2 or 3)");
            String choice = reader.nextLine();
            //starts the normal chat
            if(choice.equals("1")){
                System.out.println("do you want to be the host or connect?");
                choice = reader.nextLine();
                if(choice.equals("host")){
                        nc = new NormalChat(u);
                        nc.runServer();
                }else if(choice.equals("connect")){
                    nc = new NormalChat(u);
                    nc.runClient(nc.setHost());
                }
            }
            //starts private chat
            else if(choice.equals("2")){
                System.out.println("do you want to be the host or connect?");
                choice = reader.nextLine();
                if(choice.equals("host")){
                        pc = new PrivateChat(u);
                        pc.runServer();
                }else if(choice.equals("connect")){
                    //when connecting you must enter the ipv4 of the server
                    pc = new PrivateChat(u);
                    pc.runClient(pc.setHost());
                }
            }
            
        }else if(in.equals("N")){
            Login.run();
            User u = Login.makeUser();
            System.out.println(u);


            //run chat app
                System.out.println("do you want to chat, private chat, chat with computer (type 1, 2 or 3)");
                String choice = reader.nextLine();
                //starts the normal chat
                if(choice.equals("1")){
                    System.out.println("do you want to be the host or connect?");
                    choice = reader.nextLine();
                    if(choice.equals("host")){
                            nc = new NormalChat(u);
                            nc.runServer();
                    }else if(choice.equals("connect")){
                        nc = new NormalChat(u);
                        nc.runClient(nc.setHost());
                    }
                }
                //starts private chat
                else if(choice.equals("2")){
                    System.out.println("do you want to be the host or connect?");
                    choice = reader.nextLine();
                    if(choice.equals("host")){
                            pc = new PrivateChat(u);
                            pc.runServer();
                    }else if(choice.equals("connect")){
                        //when connecting you must enter the ipv4 of the server
                        pc = new PrivateChat(u);
                        pc.runClient(pc.setHost());
                    }
                }
                else if(choice.equals("3")){
                    Local message1 = new Local();
                    message1.input();
                }
        }
        
        
    }
}