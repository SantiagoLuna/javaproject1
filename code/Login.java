/**
 * This class is in charge of Loging the user in. When the user logs in, a new User instance is created. After it is created, the methods in the Login class are ran. Firstly the validate method 
 * is called which reads all the lines from the Users.txt file and 
 * checks if the entered credentials exist on the User.txt file. If they do, the method returns tru and the User is able to access the Chats, if not an error gets thrown at the user 
 * and the user has to run the program again or re enter their credentials.
 * If the entered credentials match those that are in the file, then the log method is called. This method appends the users' credentials along with the current date to a file called UserLog.txt.
 * After the credentials get logged, the user finally has access to the chats.
 */
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner; // Import the Scanner class to read text files

public abstract class Login implements Credentials {
    private static String PASS;
    private static String USERNAME;

    public static void getCredentials() throws IOException{
        Path p = Paths.get(".\\files\\Users.txt"); //change file name for final product
        List<String> cred = Files.readAllLines(p);
        
        //for testing
        for(String s : cred){
            System.out.println(s);
        }
    }


    //checks if the inputted username and password exists 
    //@Override
    public static boolean validate(String[] args) throws IOException{
            //boolean isValid = true;
        
            Path p = Paths.get(".\\files\\Users.txt"); //change file name for final product
            List<String> cred = Files.readAllLines(p);
            String[] credentials = cred.toArray(new String[0]);
            
            //stepping through the file comparing username and password
            
                for(int i=0; i<credentials.length-1; i+=2){
                String first = credentials[i];
                String second = credentials[i+1];

                if(first.equals(args[0]) && second.equals(args[1])){
                    System.out.println("credentials match!");
                    log(args);
                    return true;
                }
            }
                return false;
    }

    //@Override
    public static String[] input() {
        String username, password;
        String[] cred = new String[2];

        Scanner reader = new Scanner(System.in);
        System.out.println("LOGIN..........");
        System.out.println("entner username: ");
        username = reader.nextLine();
        System.out.println("entner password: ");
        password = reader.nextLine();
        reader.close();
        cred[0] = username;
        cred[1] = password;

        PASS = password;
        USERNAME = username;
        return cred;  
    }

    public static void log(String[] args) throws IOException{
        Path p = Paths.get(".\\files\\UserLog.txt");

        if(Files.exists(p)){
            List<String> fileLines = Files.readAllLines(p);
                fileLines.add(args[0] + " " + java.time.LocalDate.now());
                fileLines.add(args[1]);
                Files.write(p, fileLines);
        }else{
            List<String> credentials = new ArrayList<String>();
            credentials.add(args[0] + " " + java.time.LocalDate.now());
            credentials.add(args[1]);
            Files.write(p, credentials);
        }
    }

    public static User createUserInstance(String uname, String pass){
        User u = new User(uname, pass);
        return u;
    }

        public static User makeUser(){
            User u = new User(USERNAME,PASS);
            return u;
        }

    public static void run() throws IOException{
        boolean isValid = validate(input());
        
        while(!isValid){
            System.out.println("The user does not exist try again.....");
            isValid = validate(input());
        }
        
        
    }
    
}
