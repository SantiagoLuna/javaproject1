/**
 * @authors Santiago Luna & Maria Maria Ramirez
 * This class contains all the tests used in the program. These tests are in charge of testing the methods executed in the program and make sure their output matches the expected output. 
 * They also check the void methods complete their task correctly by looking and comparing the number of lines in files. 
 */
import org.junit.jupiter.api.Assertions.assertEquals;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.junit.jupiter.api.Test;

public class Tests {
    @Test
    void testCreateUserAddToFile() throws IOException{
        //make new user credentials
        String username = "tester";
        String password = "246";
        Path p = Paths.get(".\\files\\Users.txt");
        //count lines in the file before calling the addToFile method
        long lines = Files.lines(p).count();
        //make string array with credentials used to call addToFile method
        String[] args = {username, password};
        CreateUser.addToFile(args); // should add two more lines to file

        //count the lines in the file after calling the method with the new credentials
        long updatedLines = Files.lines(p).count();

        //make sure lines+2 == updatedLines
        assertEquals(lines+2, updatedLines);
    }

    @Test
    void testcreateUserUsername() throws IOException{
        //create a new user and aded to the User.txt file
        String username = "tester";
        String password = "246";
        String[] args = {username, password};
        CreateUser.addToFile(args);
        Path p = Paths.get(".\\files\\Users.txt");
        List<String> fileLines = Files.readAllLines(p);
        int verifier = 0;

        for(int i = 0 ; i < fileLines.size() ; i++){
            if(fileLines.get(i).equals(username)){
                verifier = 1;
            }
        }
        assertEquals(1, verifier);
    }

    @Test
    void testcreateUserPassword() throws IOException{
        //create a new user and aded to the User.txt file
        String username = "tester";
        String password = "246";
        String[] args = {username, password};
        CreateUser.addToFile(args);
        Path p = Paths.get(".\\files\\Users.txt");
        List<String> fileLines = Files.readAllLines(p);
        int verifier = 0;

        for(int i = 0 ; i < fileLines.size() ; i++){
            if(fileLines.get(i).equals(password)){
                verifier = 1;
            }
        }
        assertEquals(1, verifier);
    }

    @Test
    void testValidate() throws IOException{
        String username = "Santi";
        String password = "123";
        String[] args = {username, password};
        assertEquals(true, Login.validate(args));
    }

    @Test
    void testLog() throws IOException{
        String username = "Test";
        String password = "246";

        Path p = Paths.get("files/UserLog.txt");
        //count lines in the file before calling the addToFile method
        long lines = Files.lines(p).count();
        //make string array with credentials used to call addToFile method
        String[] args = {username, password};
        Login.log(args); // should add two more lines to file

        //count the lines in the file after calling the method with the new credentials
        long updatedLines = Files.lines(p).count();

        //make sure lines+2 == updatedLines
        assertEquals(lines+2, updatedLines);
    }

    @Test
    void testSend() throws IOException{
        String message = "testing";
        Local newMessage = new Local();
        Path p = Paths.get("../files/localChat.txt");
        //count lines in the file before calling the addToFile method
        long lines = Files.lines(p).count();
        newMessage.send(message); //should add one more line to the file
        //count lines in file after calling the method with the message
        long updatedLines = Files.lines(p).count();
        //make sure lines++ = updated lines
        assertEquals(lines+1, updatedLines);
    }

    @Test 
    void testGetUserUsername(){
        User tester = new User("tester", "testerPass");
        assertEquals("tester", tester.getUsername());
    }

    @Test 
    void testGetUserPassword(){
        User tester = new User("tester", "testerPass");
        assertEquals("testerPass", tester.getPassword());
    }

    @Test 
    void testToString(){
        User tester = new User("tester", "testerPass");
        String expectedRet = "Username: tester, password: testerPass";
        assertEquals(expectedRet, tester.toString());
    }
}